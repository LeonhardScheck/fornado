#!/usr/bin/env python3
# -*- coding: iso-8859-15 -*-
#
#   F O R N A D O _ T E S T S
#
#   2021-5 L.Scheck

from matplotlib import pyplot as plt

import numpy as np
import tensorflow as tf
from   tensorflow import keras

from fornado.toymodel_example  import toy_model, generate_training_data
from fornado.fornado_model     import setup_model
from fornado.fornado_inference import predict_fortran

adtl_ext='_native'
act='csu'

if True : # example with multiple output dimensions
    n_input  = 4
    n_output = 2
    n_hidden = 5
    n_npl = 20
    n_training = 30000    
    
else : # example with one output dimension (--> see additional test below)
    n_input  = 7
    n_output = 1
    n_hidden = 30
    n_npl = 20
    n_training = 30000

tdata = generate_training_data( n_input, n_output, n_training, seed=1 )
vdata = generate_training_data( n_input, n_output, 1000, seed=2 )
sdata = generate_training_data( n_input, n_output, 100, seed=3 )

tdata['x'].shape, tdata['y'].shape

model, model_id = setup_model( n_input=tdata['x'].shape[1], n_output=tdata['y'].shape[1], n_npl=n_npl, n_hidden=n_hidden, output_activation='linear', activation=act)

history = model.fit( tdata['x'], tdata['y'], validation_data = ( vdata['x'], vdata['y'] ),
                     epochs=20, verbose=1, batch_size=32 )

y = model.predict( sdata['x'] )
y.shape, y.mean(), y.ndim                     

y_ftn = {}
modes=['scalar','scalar_withifs', 'vector', 'vector_native', 'omp', 'omp_native']
print('ERROR wrt TF')
for mode in modes :
    y_ftn[mode] = predict_fortran( model, sdata['x'], mode=mode )
    diff = y_ftn[mode] - y
    print('{:30s} : RMSE={}, BIAS={}, MAX={}'.format( mode, np.sqrt((diff**2).mean()), diff.mean(), np.abs(diff).max() ))
    
print()
print('ERROR wrt scalar')
for mode in modes :
    if mode != 'scalar' :    
        diff = y_ftn[mode] - y_ftn['scalar']
        print('{:30s} : RMSE={}, BIAS={}, MAX={}'.format( mode, np.sqrt((diff**2).mean()), diff.mean(), np.abs(diff).max() ))

if y.ndim == 2 :
    print( (y-sdata['y']).mean(), y.mean(), sdata['y'].mean() )
    fig, ax = plt.subplots(figsize=(10,10))
    ax.scatter( y, sdata['y'], c='k', s=40 )
    ax.scatter( y_ftn['scalar'], sdata['y'], c='#0099ff', s=20 )
    ax.plot( (y.min(),y.max()), (y.min(),y.max()), 'r')
else :
    fix, ax = plt.subplots( 1, y.ndim, figsize=(5*y.ndim,5))
    for d in range(y.ndim) :
        ax[d].scatter( y[:,d], sdata['y'][:,d], c='k', s=40 )
        ax[d].scatter( y_ftn['scalar'][:,d], sdata['y'][:,d], c='g', s=20 )
        ax[d].plot( (y[:,d].min(),y[:,d].max()), (y[:,d].min(),y[:,d].max()), 'r')
fig.savefig('TF_vs_fornado.png')
        
x0 = sdata['x']
xd = x0[::-1,:]*0.0001
y0, yd = predict_fortran( model, x0, xd=xd, mode='tangent'+adtl_ext )
x0d = x0 + xd
#y0d = predict_fortran( model, x0d, mode='vector' )
y0d, _ = predict_fortran( model, x0d, xd=xd, mode='tangent'+adtl_ext )
yd_nl = y0d - y

_, xb = predict_fortran( model, x0, yb=yd, mode='adjoint'+adtl_ext )

fig, ax = plt.subplots( 1, 2, figsize=(20,10))
ax[0].plot( x0[:,0], y0, 'r' )
ax[0].plot( x0[:,0], sdata['y'], 'k' )
ax[1].scatter( yd, yd_nl)
ax[1].plot( (yd.min(),yd.max()), (yd.min(),yd.max()), 'r' )
ax[1].set_xlim((yd.min(),yd.max()))
ax[1].set_ylim((yd.min(),yd.max()))
fig.savefig('derivative_test.png')
print( abs( yd - yd_nl ).max() )
print( 'tangent    ', yd.min(), yd.max(), yd.mean() )
print( 'nonlinear  ', yd_nl.min(), yd_nl.max(), yd_nl.mean() )


print()
print('scalar product test')

np.random.seed(17)
n_samples = 1000
a = np.random.random(( n_samples, n_input ))
b = np.random.random(( n_samples, n_input ))
c = np.random.random(( n_samples, n_input )) # = x0
    
y0, La = predict_fortran( model, c, xd=a, mode='tangent'+adtl_ext )
y0, Lb = predict_fortran( model, c, xd=b, mode='tangent'+adtl_ext )
    
y0, LtLa = predict_fortran( model, c, yb=La, mode='adjoint'+adtl_ext )
y0, LtLb = predict_fortran( model, c, yb=Lb, mode='adjoint'+adtl_ext )

p1v = np.zeros(n_samples)
p2v = np.zeros(n_samples)
p3v = np.zeros(n_samples)
for s in range(n_samples) :
    p1, p2, p3 = np.dot(La[s,...],Lb[s,...]), np.dot(LtLa[s,...],b[s,...]), np.dot(a[s,...],LtLb[s,...])
    #print( s, p1, p2, p3, abs((p2-p1)/p1), abs((p3-p1)/p1) )
    p1v[s] = p1; p2v[s] = p2; p3v[s] = p3
    
fig, ax = plt.subplots(1,2,figsize=(20,5))
ax[0].scatter( p1v, p2v, c='r', s=5, alpha=0.2 )
ax[0].scatter( p1v, p3v, c='b', s=1, alpha=0.4 )
ax[0].plot( (p1v.min(),p1v.max()), (p1v.min(),p1v.max()), '--k', linewidth=0.5)
ax[1].scatter( np.arange(n_samples), abs((p2v-p1v)/p1v), c='r', s=5, alpha=0.5 )
ax[1].scatter( np.arange(n_samples), abs((p3v-p1v)/p1v), c='b', s=5, alpha=0.5 )
ax[1].set_yscale('log')
#ax[1].set_ylim((0,5e-6))
ax[1].set_title('max. rel. err. {:.1e} / {:.1e}, mean rel. err. {:.1e} / {:.1e}'.format(
                abs((p2v-p1v)/p1v).max(),  abs((p3v-p1v)/p1v).max(),
                abs((p2v-p1v)/p1v).mean(), abs((p3v-p1v)/p1v).mean() ));
ax[1].plot( (0,n_samples-1), [abs((p2v-p1v)/p1v).mean()]*2, 'r' )
ax[1].plot( (0,n_samples-1), [abs((p3v-p1v)/p1v).mean()]*2, 'b' )
fig.savefig('scalar_product_test.png')
print('max. rel. err. {:.1e} / {:.1e}, mean rel. err. {:.1e} / {:.1e}'.format(
                abs((p2v-p1v)/p1v).max(),  abs((p3v-p1v)/p1v).max(),
                abs((p2v-p1v)/p1v).mean(), abs((p3v-p1v)/p1v).mean() ))
print()

if n_output == 1 :
    np.random.seed(13)
    n_samples = 5
    a = np.random.random(( n_samples, n_input ))
    a[:,1:] = 0.0 # keep only dimension 0
    b = np.random.random(( n_samples, n_input ))
    b[:,2:] = 0.0 # keep only dimension 1
    b[:,0] = 0.0
    c = np.random.random(( n_samples, n_input )) # = x0

    y0, La = predict_fortran( model, c, xd=a, mode='tangent'+adtl_ext )
    y0, Lb = predict_fortran( model, c, xd=b, mode='tangent'+adtl_ext )

    print('La shape', La.shape, '  a shape', a.shape)
    print('a[0,:]', a[0,:])

    dydx0 = La[:,0]/a[:,0]
    dydx1 = Lb[:,0]/b[:,1]


    y0, LtLa = predict_fortran( model, c, yb=La, mode='adjoint'+adtl_ext )
    y0, LtLb = predict_fortran( model, c, yb=Lb, mode='adjoint'+adtl_ext )

    print('LtLa shape', LtLa.shape, '  La shape', La.shape)

    dydx0d = LtLa[:,0]/La[:,0]
    dydx1d = LtLa[:,1]/La[:,0]

    print()
    print( 'Derivatives computed using TL code several times')
    print( ' dydx0 = ', dydx0 )
    print( ' dydx1 = ', dydx1 )
    print()
    print( 'Derivatives computed using AD code once')
    print( ' dydx0d = ', dydx0d )
    print( ' dydx1d = ', dydx1d )
else :
    print('implement me for n_output > 1. It would require n_input * n_output TL calls to replace AD.')

benchmark=False
if benchmark :
    import cProfile
    bdata = generate_training_data( n_input, n_output, 1000000, seed=1 )
    cProfile.run("y_scalar = predict_fortran( model, bdata['x'], mode='scalar', nchunk=2048 )")
    cProfile.run("y, yd = predict_fortran( model, bdata['x'], xd=bdata['x'], mode='tangent', nchunk=2048 )")
    cProfile.run("y, xb = predict_fortran( model, bdata['x'], yb=y, mode='adjoint', nchunk=2048 )")


