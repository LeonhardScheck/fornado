# F O R N A D O

`FORNADO` (FORtran Neural network inference code including an ADjOint version) is a Fortran code including a Python interface
to perform the forward computation (inference) for feed-forward neural networks generated and trained
e.g. with Tensorflow.

Why not just use Tensorflows' predict() function?

- FORNADO is much faster than Tensorflow for small networks (but not large ones), where small means not more than about 100 nodes per layer. Vectorized and OpenMP versions are available.

  ![benchmarks results on a i5 CPU](benchmarks_i5.png)

- There is not only a forward code, but there are also tangent linear and adjoint versions (see https://journals.ametsoc.org/bams/article/78/11/2577/55799/What-Is-an-Adjoint-Model)

- FORNADO can be used in Fortran or Fortran+Python codes without a dependency on Tensorflow

For compiling the current master branch version f2py is required, the process is started with

    ./compile

A cmake-based version with an improved Fortran interface is in development.
