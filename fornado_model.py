#!/usr/bin/env python3
# -*- coding: iso-8859-15 -*-
#
#   F O R N A D O _ I O
#   Load and save neuronal network models and parameter transformation files.
#   The weights are also included in the parameter transformation pickle such
#   that they are accessible without tensorflow.
#
#   2018-10 L.Scheck

from __future__ import absolute_import, division, print_function
import os, time, pickle
import numpy as np
import tensorflow as tf
from   tensorflow import keras
from   tensorflow.keras import backend, saving
from   tensorflow.keras.layers import Activation
from   tensorflow.keras.utils import get_custom_objects

@saving.register_keras_serializable(package="fornado_package", name="actfunc_csu")
def actfunc_csu(x) :
    #return backend.switch(x >= 0, x, backend.exp(x) - 1) # ELU
    return backend.switch(x >= 0, x, backend.switch(x < -2, -1 + x*0, -1 + 0.25*(x+2)*(x+2) ) )

class NamedActivation(Activation) :
    def __init__(self, activation, name=None, **kwargs):
        super(NamedActivation, self).__init__(activation, **kwargs)
        self.__name__ = name

def define_custom_activation_functions() :
    if not 'csu' in get_custom_objects() :
        print('registering csu activation function...')
        get_custom_objects().update({'csu': NamedActivation(actfunc_csu,name='csu')})
    if not 'actfunc_csu' in get_custom_objects() :
        print('registering csu activation function...')
        get_custom_objects().update({'actfunc_csu': NamedActivation(actfunc_csu,name='actfunc_csu')})

#-----------------------------------------------------------------------------------------------------------------------
def setup_model( n_input=7, n_hidden=5, n_npl=8, n_output=1, activation='elu', output_activation='softplus',
                 learning_rate=0.001, optimizer='Adam', optimizer_type='keras', dropout=-1.0, l2reg=-1.0, loss='mse' ) :

    # define activation functions
    define_custom_activation_functions()

    if activation == 'csu' :
        act = actfunc_csu
    else :
        act = getattr( keras.activations, activation )
    output_act = getattr( keras.activations, output_activation )

    # define model structure
    model = keras.Sequential()

    # - input layer
    if l2reg > 0.0 :
        kernel_regularizer=keras.regularizers.l2(l2reg)
    else :
        kernel_regularizer=None
    model.add(     keras.layers.Dense( n_npl, activation=act, input_shape=(n_input,), kernel_regularizer=kernel_regularizer ) )
    if dropout > 0 :
        model.add( keras.layers.Dropout(dropout) )

    # - hidden layers
    for l in range(1,n_hidden) :
        if l2reg > 0.0 :
            kernel_regularizer=keras.regularizers.l2(l2reg)
        else :
            kernel_regularizer=None
        model.add( keras.layers.Dense( n_npl, activation=act, kernel_regularizer=kernel_regularizer ) )
        if dropout > 0 :
            model.add( keras.layers.Dropout(dropout) )

    # - output layer
    model.add(     keras.layers.Dense( n_output, activation=output_act) )

    # define optimizer
    if optimizer_type == 'keras' :
        opt = getattr( keras.optimizers, optimizer )(lr=learning_rate)
    elif optimizer_type == 'tf' :
        opt = getattr( tf.train, optimizer+'Optimizer' )(learning_rate)

    # compile model
    metrics = ['mae']
    if not isinstance(loss, str) or loss != 'mse' :
        metrics.append('mse')
    if not isinstance(loss, str) or loss != 'mape' :
        metrics.append('mape')
    print('>>> using loss function ', loss)
    model.compile( loss=loss, optimizer=opt, metrics=metrics )

    # construct id describing the model
    #model_id = 'L{}_N{}_ACT{}_OACT{}_OPT{}{}_LR{:.5f}'.format( n_hidden, n_npl, activation, output_activation,
    model_id = 'L{}_N{}_ACT{}_OACT{}_OPT{}{}_LR{:.1e}'.format( n_hidden, n_npl, activation, output_activation,
                                                               optimizer_type, optimizer, learning_rate )
    if dropout > 0 :
        if dropout > 1e-2 :
            model_id += '_DROP{:.3f}'.format(dropout)
        else :
            model_id += '_DROP{:.3e}'.format(dropout)

    if l2reg > 0 :
        model_id += '_L2REG{:.0e}'.format(l2reg)

    return model, model_id

#-----------------------------------------------------------------------------------------------------------------------
def model_filenames( id ) :
    #fname = os.path.expanduser('~/fornado/nn/models/model_')+id  # FIXME
    fname = '/project/meteo/work/Leonhard.Scheck/fornado/nn/models/model_'+id
    return fname+'.h5', fname+'.partraf.pickle'

#-----------------------------------------------------------------------------------------------------------------------
def model_exists( id ) :
    fnm, fnp = model_filenames( id )
    if os.path.exists(fnm) and os.path.exists(fnp) :
        return True
    else :
        return False

#-----------------------------------------------------------------------------------------------------------------------
def save_model( model, partraf, history, etimes, id, pickle_only=False ) :
    fnm, fnp = model_filenames( id )

    if not pickle_only :
        print('SAVING MODEL TO {} ...'.format(fnm))
        model.save(fnm)

    print('SAVING PARAMETER TRANSFORMATION INFORMATION AND MODEL DEFINITION TO {}...'.format(fnp))
    modeldef = keras2fortran( model )
    with open(fnp,'wb') as f :
        pickle.dump( {'modeldef':modeldef, 'partraf':partraf, 'history':history, 'etimes':etimes}, f, protocol=2 )
        # protocol=2 is used for Python2 compatibility

#-----------------------------------------------------------------------------------------------------------------------
def load_model( id, use_keras=True, verbose=False ) :
    fnm, fnp = model_filenames( id )

    if verbose : print('LOADING PARAMETER TRANSFORMATION INFORMATION AND MODEL DEFINITION FROM {}...'.format(fnp))
    with open(fnp,'rb') as f :
        d = pickle.load( f )

    if use_keras :
        from tensorflow import keras
        if verbose : print('LOADING KERAS MODEL OBJECT FROM {} ...'.format(fnm))
        model = keras.models.load_model(fnm)
        return model, d['partraf'], d['history'], d['etimes']

    else :
        if 'modeldef' in d :
            return d['modeldef'], d['partraf'], d['history'], d['etimes']
        else :
            print('WARNING: {} does not contain the model definition!'.format(fnp))
            return None, d['partraf'], d['history'], d['etimes']
            
#-----------------------------------------------------------------------------------------------------------------------
def keras2fortran( model, verbose=False ) :
    """Convert keras model object to a dictionary with numpy arrays and integers to be used for fortran inference code"""

    n_layers     = len(model.layers)
    n_hidden     = n_layers - 1
    if int(tf.__version__[0]) >= 2 :
        n_nodes_max  = np.array([l.weights[1].shape[0] for l in model.layers if 'Dense' in str(type(l)) ]).max()
        #                                                                    (to avoid Dropout layers)
    else :
        n_nodes_max  = np.array([l.weights[1].shape[0].value for l in model.layers if 'Dense' in str(type(l)) ]).max()
    if verbose : print('found {} layers with max. {} nodes'.format(n_layers,n_nodes_max) )

    if int(tf.__version__[0]) >= 2 :
        n_input  = model.layers[0].weights[0].shape[0]
        n_output = model.layers[-1].weights[1].shape[0]
    else :
        n_input  = model.layers[0].weights[0].shape[0].value
        n_output = model.layers[-1].weights[1].shape[0].value
    if verbose : print('n_input = {}, n_output = {}'.format(n_input,n_output) )

    actname2number = { 'linear':0, 'relu':1, 'elu':2, 'softplus':3, 'tanh':4, 'csu':99 }
    act_h = actname2number[ model.layers[0].activation.__name__.replace('actfunc_','') ]
    act_o = actname2number[ model.layers[-1].activation.__name__.replace('actfunc_','') ]

    weight_i = np.zeros(( n_input,     n_nodes_max             ), dtype=np.float32, order='F')
    weight_h = np.zeros(( n_nodes_max, n_nodes_max, n_hidden-1 ), dtype=np.float32, order='F')
    weight_o = np.zeros(( n_nodes_max, n_output                ), dtype=np.float32, order='F')

    bias_i   = np.zeros((              n_nodes_max             ), dtype=np.float32, order='F')
    bias_h   = np.zeros((              n_nodes_max, n_hidden-1 ), dtype=np.float32, order='F')
    bias_o   = np.zeros((              n_output                ), dtype=np.float32, order='F')

    weights = model.get_weights()

    i = 0
    for l in model.layers :
        if not 'Dense' in str(type(l)) : # avoid Dropout layers...
            continue
        if verbose : print( i, l.activation.__name__, l.weights[0].shape, l.weights[1].shape )
        if i == 0 :
            weight_i = weights[i*2]
            bias_i   = weights[i*2+1]
        elif i < n_layers-1 :
            weight_h[:,:,i-1] = weights[i*2]
            bias_h[    :,i-1] = weights[i*2+1]
        else :
            weight_o = weights[i*2]
            bias_o   = weights[i*2+1]
        i = i+1

    return {'n_layers':n_layers, 'n_hidden':n_hidden,'n_nodes_max':n_nodes_max,
            'n_input':n_input, 'n_output':n_output, 'act_h':act_h, 'act_o':act_o,
            'weight_i':weight_i, 'weight_h':weight_h,  'weight_o':weight_o,
            'bias_i':  bias_i,   'bias_h':  bias_h,    'bias_o':  bias_o}

#-----------------------------------------------------------------------------------------------------------------------
def keras2fortran_multi( models, verbose=False ) :
    """Convert several keras model objects to a dictionary with numpy arrays and integers to be used for fortran inference code"""

    n_networks = len(models)
    model = models[0]

    n_layers     = len(model.layers)
    n_hidden     = n_layers - 1
    n_nodes_max  = np.array([l.weights[1].shape[0].value for l in model.layers if 'Dense' in str(type(l)) ]).max()
    #                                                                          (to avoid Dropout layers)
    if verbose : print('found {} layers with max. {} nodes'.format(n_layers,n_nodes_max) )
    n_input  = model.layers[0].weights[0].shape[0].value
    n_output = model.layers[-1].weights[1].shape[0].value
    if verbose : print('n_input = {}, n_output = {}'.format(n_input,n_output) )

    actname2number = { 'linear':0, 'relu':1, 'elu':2, 'softplus':3, 'tanh':4, 'csu':99 }
    act_h = actname2number[ model.layers[0].activation.__name__ ]
    act_o = actname2number[ model.layers[-1].activation.__name__ ]

    weight_i = np.zeros(( n_input,     n_nodes_max,             n_networks ), dtype=np.float32, order='F')
    weight_h = np.zeros(( n_nodes_max, n_nodes_max, n_hidden-1, n_networks ), dtype=np.float32, order='F')
    weight_o = np.zeros(( n_nodes_max, n_output,                n_networks ), dtype=np.float32, order='F')

    bias_i   = np.zeros((              n_nodes_max,             n_networks ), dtype=np.float32, order='F')
    bias_h   = np.zeros((              n_nodes_max, n_hidden-1, n_networks ), dtype=np.float32, order='F')
    bias_o   = np.zeros((              n_output,                n_networks ), dtype=np.float32, order='F')

    for i_model, model in enumerate(models) :
        weights = model.get_weights()
        #print('------------------ model {} ---------------------'.format(i_model))
        i = 0
        for l in model.layers :
            if not 'Dense' in str(type(l)) : # avoid Dropout layers...
                continue
            if verbose and i_model == 0 : print( i, l.activation.__name__, l.weights[0].shape, l.weights[1].shape )
            if i == 0 :
                weight_i[...,i_model] = weights[i*2]
                bias_i[...,i_model]   = weights[i*2+1]
            elif i < n_layers-1 :
                weight_h[:,:,i-1,i_model] = weights[i*2]
                bias_h[    :,i-1,i_model] = weights[i*2+1]
            else :
                weight_o[...,i_model] = weights[i*2]
                bias_o[...,i_model]   = weights[i*2+1]
            i = i+1

    return {'n_networks':n_networks,
            'n_layers':n_layers, 'n_hidden':n_hidden,'n_nodes_max':n_nodes_max,
            'n_input':n_input, 'n_output':n_output, 'act_h':act_h, 'act_o':act_o,
            'weight_i':weight_i, 'weight_h':weight_h,  'weight_o':weight_o,
            'bias_i':  bias_i,   'bias_h':  bias_h,    'bias_o':  bias_o}

#-----------------------------------------------------------------------------------------------------------------------
def define_parser() :
    parser = argparse.ArgumentParser(description='Evaluate neural networks')
    parser.add_argument( '--use-keras',             dest='use_keras',         action='store_true',  help='load model in keras format' )
    parser.add_argument( '--write-pickle',          dest='write_pickle',      action='store_true',  help='load model in keras format, save it as pickle' )
    parser.add_argument( '--model-id',              dest='model_id',
                          default='L6_N23_ACTelu_OACTsoftplus_OPTkerasAdam_LR0.00050__data_PARTRAF-A0_VLA_S1_C1_DISORT-VIS006_L3261CUN_g2r1__training__tf0.010_vf0.010_e1000_b256',                          
                          help='default model id' )
                          #default='L6_N23_ACTelu_OACTsoftplus_OPTkerasAdam_LR0.00050__data_PARTRAF-A0_VLA_S1_C1_DISORT-VIS006_L3261CUN_g2r1__training__tf0.010_vf0.010_e1000_b256',
                          #default='L5_N26_ACTelu_OACTsoftplus_OPTkerasAdam_LR0.00050__data_PARTRAF-A0_VLA_S1_C1_DISORT-VIS006_L3261CUN_g2r1__training__tf0.010_vf0.010_e10_b256',                        
                          #default='L5_N26_ACTelu_OACTsoftplus_OPTkerasAdam_LR0.00050__data_PARTRAF-A0_VLA_S1_C1_DISORT-VIS006_L3261CUN_g2r1__training__tf0.010_vf0.010_e1000_b256',
    return parser                        

#-----------------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
#-----------------------------------------------------------------------------------------------------------------------

    import argparse
    parser = define_parser()
    args = parser.parse_args()

    use_keras = args.use_keras or args.write_pickle
    model, partraf, history, etimes = load_model( args.model_id, use_keras=use_keras )
    if use_keras :
        modeldef = keras2fortran( model )
    else :
        modeldef = model

    print()
    print( 'model'+('-'*50))
    print( modeldef )
    print()
    print( 'partraf'+('-'*50))
    print( partraf )

    if args.write_pickle :
        save_model( model, partraf, history, etimes, args.model_id, pickle_only=True )

