#!/usr/bin/env python3
# -*- coding: iso-8859-15 -*-
#
#   F O R N A D O _ B E N C H M A R K S
#   compare performance of different inference methods for different network sizes
#
#   Example:
#   fornado_benchmarks.py --suffix macbook-pro-i5 --description="i5-1038NG7 quad-core @ 2.00GHz" -s 0.5 -p 24 -b 5
#   fornado_benchmarks.py --suffix macbook-pro-i5-s1p12b5-tsvn --description="i5-1038NG7 quad-core @ 2GHz, gfortran 10.2" -l 5 -s 1 -p 12 -b 5
#
#   2020-9 L.Scheck

from matplotlib import pyplot as plt
import numpy as np
from time import perf_counter
import argparse, pickle, sys

import tensorflow as tf
from   tensorflow import keras

from fornado.fornado_model     import setup_model, keras2fortran, define_custom_activation_functions
from fornado.fornado_inference import predict_fortran

#-----------------------------------------------------------------------------------------------------------------------
def define_parser() :
    parser = argparse.ArgumentParser(description='Perform FORNADO benchmarks')

    parser.add_argument( '-m', '--methods',       dest='methods', default='TF,scalar,vector,vector_native', help='comma-separated list of methods' )
    parser.add_argument( '-l', '--layers',        dest='n_hidden',      type=int,   default=10,   help='number of hidden layers' )
    parser.add_argument( '-s', '--samples',       dest='samples_scale', type=float, default=1.0,  help='scaling factor for number of samples' )
    parser.add_argument( '-a', '--activation',    dest='activation',    type=str,   default='elu',  help='activation function (default: elu)' )
    parser.add_argument(       '--adtl',          dest='adtl',          action='store_true',      help='add AD/TL lines for first n_params, first activation function' )
    parser.add_argument( '-S', '--suffix',        dest='suffix',                    default=None, help='file name suffix' )
    parser.add_argument(       '--start-from',    dest='start_from',   default=None, help='file name of pickle from previous run that should be continued' )
    parser.add_argument( '-d', '--description',   dest='description',               default=None, help='description to be added to plot' )
    parser.add_argument(       '--scale-samples', dest='scale_nsamples', action='store_true',     help='compare results with and without keras' )
    parser.add_argument( '-b', '--bestof',        dest='bestof',         type=int,  default=5,    help='number times to repeat each measurement' )
    parser.add_argument( '-p', '--points',        dest='n_points',       type=int,  default=24,   help='number points in each line' )
    
    parser.add_argument(       '--nparams',       dest='n_params',       default=None,            help='comma-separated list of parameter numbers' )
    parser.add_argument(       '--n-npls',        dest='n_npls',         default=None,            help='comma-separated list of the number of nodes per layer')
    parser.add_argument(       '--n-hidden',      dest='n_hidden',       type=int, default=8,     help='comma-separated list of the number of nodes per layer')

    parser.add_argument(       '--param-table',   dest='param_table',   action='store_true',      help='print out table nparams(hidden layers, nodes/layer) for specified n_input, n_output' )
    parser.add_argument(       '--n-input',       dest='n_input',        type=int,  default=8,    help='number of input parameters for table')
    parser.add_argument(       '--n-output',      dest='n_output',       type=int,  default=3,    help='number of output parameters for table')

    return parser

#-----------------------------------------------------------------------------------------------------------------------
def perform_benchmark( methods=None, bestof=5, n_samples=1000, n_input=3, n_hidden=5, n_npl=8, n_output=3, activation='elu', wtps=None, **mparams ) :

    if methods is None :
        methods = ['TF', 'scalar','vector','omp']

    # set up model
    define_custom_activation_functions()
    model, model_id = setup_model( n_input=n_input, n_hidden=n_hidden, n_npl=n_npl, n_output=n_output, activation=activation, **mparams )

    # generate random data
    data = generate_random_training_data( n_input, n_output, n_samples )

    # perform some pseudo-training just to initialize the weights    
    history = model.fit( data['x'], data['y'], epochs=1, batch_size=256 )

    # call inference methods
    m = keras2fortran( model, verbose=False ) # convert weights and biases to fortran format
    y_inf = {}
    if wtps is None : wtps  = {}
    for method in methods :
        if not method in wtps : wtps[method] = 1e15

        for b in range(bestof) :    
            if method == 'TF' :
                n_tf = n_samples #//10
                t0 = perf_counter()
                y_inf[method] = model.predict( data['x'][:n_tf,:] )
                t1 = perf_counter()
                dtn = (t1-t0)/n_tf
            elif method in ['adjoint','tangent'] :
                t0 = perf_counter()
                y_inf[method], _ = predict_fortran( m, data['x'], mode=method+'_native' )
                t1 = perf_counter()
                dtn = (t1-t0)/n_samples
            else :
                t0 = perf_counter()
                y_inf[method] = predict_fortran( m, data['x'], mode=method )
                t1 = perf_counter()
                dtn = (t1-t0)/n_samples
            print('      . try #{} : {}sec -> {}sec/sample'.format( b, t1-t0, dtn ) )
            wtps[method] = np.minimum( dtn, wtps[method] )
            
        #print( '>>>>> - {:10s} ({:6.3f}sec) : first element {}, mean error {}, max error {}'.format( method, t1-t0, y_inf[method].ravel()[0],
        #        abs(y_inf[method].ravel()-data['y'].ravel()).mean(), abs(y_inf[method].ravel()-data['y'].ravel()).max() ) )
        print( '----- . {:10s} : first element {}'.format( method, y_inf[method].ravel()[0] ) )

    return wtps

#-----------------------------------------------------------------------------------------------------------------------
def generate_random_training_data( n_input, n_output, n_samples, seed=5555, dtype=np.float32 ) :
    np.random.seed(seed)
    x = np.zeros(( n_samples, n_input  ), dtype=dtype, order='F' )
    y = np.zeros(( n_samples, n_output ), dtype=dtype, order='F' )
    for i in range(n_samples) :
        x[i,:] = np.random.random( n_input  )
        y[i,:] = np.random.random( n_output )
    return {'x':x, 'y':y}

#-----------------------------------------------------------------------------------------------------------------------
def n_params( npl=1, n_input=1, n_hidden=1, n_output=1 ) :
    """Compute number of paramers for a NN with npl nodes in each hidden layer"""
    return npl*npl*(n_hidden-1) + npl*(n_input + n_output + n_hidden) + n_output

#-----------------------------------------------------------------------------------------------------------------------
def nodes_per_layer( n_par, n_input=1, n_hidden=1, n_output=1 ) :
    """Compute number of nodes per hidden layer (assuming it is the same in each hidden layer)"""
    if n_hidden > 1 :
        a = n_hidden - 1.0
        b = n_input + n_hidden + n_output
        c = n_output - n_par
        q = b**2 - 4*a*c
        if q <= 0 :
            return 0
        else :
            return int(round( (-b + np.sqrt(q))/(2*a) ))
    elif n_hidden == 1 :
        return int(round( (n_par-n_output)/(n_input+n_output+1) ))
    else :
        raise ValueError('n_hidden must be > 0')


#----------------------------------------------------------------------------------------------------------------------
if __name__ == "__main__": # ------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------

    parser = define_parser()
    args = parser.parse_args()

    if args.param_table :
        print()
        print('n_params(nodes per layer, number of hidden layers) for n_input={}, n_output={}'.format(args.n_input,args.n_output))
        print('n/l | ', end='')
        for n_hidden in range(2,11) :
            print('{:6d} | '.format(n_hidden+0), end='')
        print()
        print('-'*86)
        for n_npl in range(5,201) :
            print('{:3d} | '.format(n_npl+0), end='')
            for n_hidden in range(2,11) :
                n_par = n_params( npl=n_npl, n_input=args.n_input, n_hidden=n_hidden, n_output=args.n_output )
                if n_par < 1e7 :
                    print('{:6d} | '.format(n_par), end='')
                else :
                    print('>=1e7  | ', end='')
            print()
        print()
        sys.exit(0)


    #methods = ['TF','scalar','scalar_native','vector','vector_native','omp_native']
    methods = args.methods.split(',')

    if args.n_params is None : # increase nodes per layer (and thus also number of network parameters)

        if args.n_npls is None :
            n_npls = [ int(n) for n in 10**np.linspace( 1, 3, args.n_points) ]
        else :
            n_npls = [ int(n) for n in args.n_npls.split(',') ]

        wtps = {}
        for n_npl in n_npls :
            print()
            print('>>>>> NODES PER LAYER : ', n_npl)
            n_samples = int( 1e6 * args.samples_scale * (10**2) / (n_npl**1.6) )
            wtps[n_npl] = perform_benchmark( n_input=args.n_input, n_output=args.n_output, n_hidden=args.n_hidden, n_npl=n_npl,
                                            n_samples=n_samples, methods=methods, bestof=args.bestof, activation=args.activation )
            print( '>>>>>> ', n_npl, n_samples, wtps[n_npl])

        bfname = 'benchmark_{}layers{}'.format( args.n_hidden, '_'+args.suffix if not args.suffix is None else '')

        fig, ax = plt.subplots()
        for m in methods :
            ax.plot( n_npls, [wtps[n_npl][m] for n_npl in n_npls ], '.-', label=m )
        ax.grid(alpha=0.3)
        ax.set_ylim(bottom=0)
        ax.legend( title='{} layers'.format(args.n_hidden), frameon=False )
        ax.set_title( args.description )
        ax.set_xlabel('nodes per layer')
        ax.set_ylabel('seconds per sample')
        fig.savefig( bfname+'.svg', bbox_inches='tight' )
        fig.savefig( bfname+'.png', bbox_inches='tight' )

        fmt_str = ''.join(['{:>15s} | ']*(len(methods)+1)) + '\n'
        fmt     = '{:>15d} | ' + (''.join(['{:15.5e} | ']*len(methods))) + '\n'
        with open( bfname+'.txt','w') as f :
            f.write('FORNADO BENCHMARKS\n')
            f.write( fmt_str.format( *(['nodes/layer'] + methods) ) )
            for n_npl in n_npls :
                f.write( fmt.format( *([n_npl] + [wtps[n_npl][m] for m in methods]) ) )

    else : # keep number of network parameters fixed, increase number of hidden layers

        n_input  = 7
        n_output = 1
        n_pars = [ int(s) for s in args.n_params.split(',') ]
        n_hiddens = [ 2, 3, 4, 5, 6, 8, 10, 12, 15 ]
        #n_hiddens = np.arange( 2, 16 )
        print('Performing benchmarks for ', n_pars, ' parameters and ', n_hiddens, ' hidden layers...')

        acts = args.activation.split(',')

        n_samples = int( 1e6 * args.samples_scale )

        if args.start_from :
            print('Reading results from previous experiment, ', args.start_from)
            with open(args.start_from,'rb') as f :
                res = pickle.load(f) 
            wtps = res['wtps']
            wtps_adtl = res['wtps_adtl']
        else :
            wtps = {}
            wtps_adtl = {}

        for n_par in  n_pars :
            if not n_par in wtps : wtps[n_par] = {}

            for n_hidden in n_hiddens :
                if not n_hidden in wtps[n_par] : wtps[n_par][n_hidden] = {}

                n_npl = nodes_per_layer( n_par, n_input=n_input, n_hidden=n_hidden, n_output=n_output )                
                for act in acts :
                    print('=========================== n_par={}, n_hidden={}, n_npl={}, activation={} ==========================='.format(n_par,n_hidden,n_npl,act))
                    wtps[n_par][n_hidden][act] = perform_benchmark( n_input=n_input, n_output=n_output, n_hidden=n_hidden, n_npl=n_npl,
                                                            n_samples=n_samples, methods=methods, bestof=args.bestof, activation=act,
                                                            wtps = wtps[n_par][n_hidden][act] if act in wtps[n_par][n_hidden] else None )
                    print( '>>>>>> ', n_npl, n_samples, wtps[n_par][n_hidden] )

                if args.adtl and n_par == n_pars[0] :
                    wtps_adtl[n_hidden] = perform_benchmark( n_input=n_input, n_output=n_output, n_hidden=n_hidden, n_npl=n_npl,
                                            n_samples=n_samples, methods=['adjoint','tangent'], bestof=args.bestof, activation=acts[0] )
                    print( '|||||| ', n_npl, n_samples, wtps_adtl[n_hidden] )

        bfname = 'benchmark_m{}__n{}{}'.format( '-'.join(methods), '_'.join(['{}'.format(i) for i in n_pars]), '_'+args.suffix if not args.suffix is None else '')
        
        print('saving results to {}.pickle ...'.format(bfname))
        with open(bfname+'.pickle','wb') as f :
            pickle.dump( {'wtps':wtps, 'wtps_adtl':wtps_adtl}, f )

        print('plotting...')
        fig, ax = plt.subplots(figsize=(5,3))
        for i_par, n_par in enumerate(n_pars) :
            n_hiddens = list(wtps[n_par].keys())
            for m in methods :
                for iact, act in enumerate(acts) :
                    if len(methods) > 1 :
                        lbl = '{} P={}'.format(m,n_par)
                    else :
                        lbl = '{} '.format(n_par)
                    if len(acts) > 1 :
                        lbl += ' '+act
                    lw = [2.5,1][iact]                    
                    ls = ['-','--',':','-.'][i_par] + 'k'
                    ax.plot( n_hiddens, [ wtps[n_par][n_hidden][act][m]/1e-6 for n_hidden in n_hiddens ], ls, linewidth=lw, label=lbl )
            if args.adtl :
                for m in ['adjoint','tangent'] :
                    lbl = '{} {} {}'.format(n_pars[0],acts[0],m)
                    ls=':k'
                    lw = 2.5 if m == 'adjoint' else 1
                    ax.plot( n_hiddens, [ wtps_adtl[n_hidden][m]/1e-6 for n_hidden in n_hiddens ], ls, linewidth=lw, label=lbl )

        ax.set_xticks(n_hiddens)
        ax.grid(alpha=0.25)
        ax.set_ylim(bottom=0)
        ax.legend( title=args.description, frameon=True, loc='lower right' )
        #ax.set_title( args.description )
        ax.set_xlabel('number of hidden layers')
        ax.set_ylabel('microseconds per sample')
        for ext in ['.png','.svg','.pdf'] :
            fig.savefig( bfname + ext, bbox_inches='tight' )

