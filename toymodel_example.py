#!/usr/bin/env python3
# -*- coding: iso-8859-15 -*-
#
#   T O Y M O D E L
#   example for training a network with data from a simple toy model (with adjustable input and output dimensions)
#   with tensorflow and using fornado for inference
#
#   2020-9 L.Scheck

from matplotlib import pyplot as plt
import numpy as np
import tensorflow as tf
from   tensorflow import keras

from fornado.fornado_model     import setup_model, keras2fortran
from fornado.fornado_inference import predict_fortran

#-----------------------------------------------------------------------------------------------------------------------
def toy_model( x, n_output=1 ) :
    n_input = x.size
    res = np.zeros(n_output)
    res = [ np.array([ np.cos(x[i]*(i+1)*(j+1)) for i in range(n_input) ]).sum() for j in range(n_output)]
    return res

#-----------------------------------------------------------------------------------------------------------------------
def generate_training_data( n_input, n_output, n_samples, seed=5555, dtype=np.float32 ) :
    np.random.seed(seed)
    x = np.zeros(( n_samples, n_input  ), dtype=dtype, order='F' )
    y = np.zeros(( n_samples, n_output ), dtype=dtype, order='F' )
    for i in range(n_samples) :
        x[i,:] = np.random.random( n_input )
        y[i,:] = toy_model( x[i,:], n_output=n_output)
    return {'x':x, 'y':y}

#----------------------------------------------------------------------------------------------------------------------
if __name__ == "__main__": # ------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------

    #n_input  = 7
    #n_output = 1
    n_input  = 4
    n_output = 2

    shuffle_and_extract = True

    if shuffle_and_extract : # extract training and validation data from common data set with randomized order

        data = generate_training_data( n_input, n_output, 30000, seed=1 )       
        x = data['x']
        y = data['y']
        print('>>>>> shapes of x, y : ', x.shape, y.shape)

        print('>>>>> randomizing data order...' )
        np.random.seed(17)
        order = np.argsort(np.random.random(y.shape[0]))
        np.take( x, order, axis=0, out=x )
        np.take( y, order, axis=0, out=y )
        print('>>>>> shapes of x, y after shuffling: ', x.shape, y.shape)

        print('>>>>> extracting subsets...')
        frac_t = 0.333
        frac_v = 0.333
        ifrac_t = int(round( frac_t * y.shape[0] ))
        ifrac_v = int(round( frac_v * y.shape[0] ))
        tdata = { 'x':x[ :ifrac_t,...], 'y':y[ :ifrac_t,...] } # use begin of data vector
        vdata = { 'x':x[-ifrac_v:,...], 'y':y[-ifrac_v:,...] } # use end   of data vector

    else : # generate training and validation data separately

        tdata = generate_training_data( n_input, n_output, 10000, seed=1 )
        vdata = generate_training_data( n_input, n_output, 10000, seed=2 )

    print('>>>>> shapes of training   x, y : ', tdata['x'].shape, tdata['y'].shape )
    print('>>>>> shapes of validation x, y : ', vdata['x'].shape, vdata['y'].shape )

    model, model_id = setup_model( n_input=tdata['x'].shape[1], n_output=tdata['y'].shape[1],
                                   n_npl=10, n_hidden=5, activation='elu', output_activation='linear' )

    history = model.fit( tdata['x'], tdata['y'], validation_data = ( vdata['x'], vdata['y'] ),
                     epochs=100, verbose=1, batch_size=32 )

    print('>>>>> applying TF predict on training and validation data:' )
    for mdata in (tdata,vdata) :
        x       = mdata['x']
        y_exact = mdata['y']
        y       = model.predict( x )
        print( '>>>>> - mean absolute error, mean value : ', abs(y-y_exact).mean(), y.mean() )

    print( '>>>>> converting model from keras to fortran format...' )
    m = keras2fortran(model,verbose=True)

    print('>>>>> applying TF predict routine / Fortran inference code on training data:' )
    y_ftn = {}
    for mode in ['TF', 'scalar','vector','omp'] : #,'dbg'
        if mode == 'TF' :
            y_ftn[mode] = model.predict( tdata['x'] )
        else :
            y_ftn[mode] = predict_fortran( m, tdata['x'], mode=mode )
        print( '>>>>> - {:10s} : first element {}, mean error {}, max error {}'.format( mode, y_ftn[mode].ravel()[0],
                abs(y_ftn[mode].ravel()-tdata['y'].ravel()).mean(), abs(y_ftn[mode].ravel()-tdata['y'].ravel()).max() ) )


    print('>>>>> generating random profile plots...' )
    N = 100
    x_prof = np.zeros((N,n_input))
    for i in range(n_input) :
        x_prof[:,i] = np.linspace( np.random.random(), np.random.random(), N )    

    y_prof_exact = np.zeros((N,n_output))
    for i in range(N) :
        y_prof_exact[i,:] = toy_model( x_prof[i,:], n_output=n_output )
    y_prof_tf    =  model.predict( x_prof )
    y_prof_ftn   =  predict_fortran( m, x_prof )

    fig, ax = plt.subplots( n_input, n_output, figsize=(n_input*3,n_output*3) )
    for i in range(n_input) :
        for j in range(n_output) :
            ax[i,j].plot( x_prof[:,i], y_prof_exact[:,j], 'k', linewidth=2 )
            ax[i,j].plot( x_prof[:,i], y_prof_tf[:,j], '--r' )
            ax[i,j].plot( x_prof[:,i], y_prof_ftn[:,j], ':b' )
    fig.savefig('toy_model_profile.png', bbox_inches='tight')
    plt.close(fig)

    print('>>>>> performing scalar product test for adjoint / tangent linear code...')
    np.random.seed(17)
    n_samples = 1000
    a = np.random.random(( n_samples, n_input ))
    b = np.random.random(( n_samples, n_input ))
    c = np.random.random(( n_samples, n_input )) # = x0
        
    y0, La = predict_fortran( model, c, xd=a, mode='tangent' )
    y0, Lb = predict_fortran( model, c, xd=b, mode='tangent' )
        
    y0, LtLa = predict_fortran( model, c, yb=La, mode='adjoint' )
    y0, LtLb = predict_fortran( model, c, yb=Lb, mode='adjoint' )

    p1v = np.zeros(n_samples)
    p2v = np.zeros(n_samples)
    p3v = np.zeros(n_samples)
    for s in range(n_samples) :
        p1, p2, p3 = np.dot(La[s,...],Lb[s,...]), np.dot(LtLa[s,...],b[s,...]), np.dot(a[s,...],LtLb[s,...])
        #print( s, p1, p2, p3, abs((p2-p1)/p1), abs((p3-p1)/p1) )
        p1v[s] = p1; p2v[s] = p2; p3v[s] = p3
        
    fig, ax = plt.subplots(1,2,figsize=(20,5))
    ax[0].scatter( p1v, p2v, c='r', s=5, alpha=0.2 )
    ax[0].scatter( p1v, p3v, c='b', s=1, alpha=0.4 )
    ax[0].plot( (p1v.min(),p1v.max()), (p1v.min(),p1v.max()), '--k', linewidth=0.5)
    ax[1].scatter( np.arange(n_samples), abs((p2v-p1v)/p1v), c='r', s=5, alpha=0.5 )
    ax[1].scatter( np.arange(n_samples), abs((p3v-p1v)/p1v), c='b', s=5, alpha=0.5 )
    ax[1].set_yscale('log')
    #ax[1].set_ylim((0,5e-6))
    ax[1].set_title('max. rel. err. {:.1e} / {:.1e}, mean rel. err. {:.1e} / {:.1e}'.format(
                    abs((p2v-p1v)/p1v).max(),  abs((p3v-p1v)/p1v).max(),
                    abs((p2v-p1v)/p1v).mean(), abs((p3v-p1v)/p1v).mean() ))
    ax[1].plot( (0,n_samples-1), [abs((p2v-p1v)/p1v).mean()]*2, 'r' )
    ax[1].plot( (0,n_samples-1), [abs((p3v-p1v)/p1v).mean()]*2, 'b' )

    fig.savefig('toy_model_ADTL_test.png', bbox_inches='tight')

