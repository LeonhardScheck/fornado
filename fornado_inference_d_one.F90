!----------------------------------------------------------------------------------------------
!
!   FORNADO NEURAL NETWORK INFERENCE
!   2019-10 L.Scheck
!
!   One sample version of fornado_inference_d.F90
!
!   to generate a python module from this subroutine use
!     f2py -c fornado_inference_d_one.F90 -m fornado_inference_d_one_f90 --f90flags="-O3"
!   (for a debug version, use
!     f2py -c fornado_inference_d_one.F90 -m fornado_inference_d_one_f90 --f90flags="-g -fcheck='all'"
!   (see http://cens.ioc.ee/projects/f2py2e/usersguide/f2py_usersguide.pdf )
!
!----------------------------------------------------------------------------------------------
subroutine fornado_inference_d( n_input, n_output, n_hidden, n_nodes,  &
                                           act_h,    act_o,             &
                                 weight_i, weight_h, weight_o,          &
                                 bias_i,   bias_h,   bias_o,            &
                                 x, xd, y, yd )

  ! tangent linear version of fornado_inference.F90

  implicit none
  integer, parameter :: sp = kind(1.0)

  integer, intent(in) :: n_input, n_output, n_hidden, n_nodes

  integer, intent(in) :: act_h, act_o

  real(sp), dimension( n_input, n_nodes ),             intent(in) :: weight_i
  real(sp), dimension( n_nodes, n_nodes, n_hidden-1 ), intent(in) :: weight_h
  real(sp), dimension( n_nodes, n_output ),            intent(in) :: weight_o

  real(sp), dimension( n_nodes ),                      intent(in) :: bias_i
  real(sp), dimension( n_nodes, n_hidden-1 ),          intent(in) :: bias_h
  real(sp), dimension( n_output ),                     intent(in) :: bias_o

  real(sp), dimension( n_input  ),          intent(in) :: x, xd
  real(sp), dimension( n_output ),       intent(inout) :: y, yd

  real(sp), dimension( n_nodes ) :: sigi, sigo, sigid, sigod
  logical,  dimension( n_nodes ) :: pos

  integer :: i, j, l

  ! propagate signal from input layer to first hidden layer ....................
  do i=1,n_nodes
     sigod(i) = 0.0
     sigo(i)  = bias_i(i)
     do j=1,n_input
        sigod(i) = sigod(i) + weight_i(j, i) * xd(j)
        sigo(i)  = sigo(i)  + weight_i(j, i) * x(j)
     end do
  end do

  ! apply activation function
  call actfunc_d( act_h, sigo, sigod, sigi, sigid )

  ! propagate signal towards last hidden layer .................................
  do l=1, n_hidden-1

     ! progagate from layer l to l+1 (l=1 is first hidden layer)
     do i=1,n_nodes
        sigod(i) = 0.0
        sigo(i) = bias_h(i, l)
        do j=1,n_nodes
           sigod(i) = sigod(i) + weight_h(j, i, l) * sigid(j)
           sigo(i)  = sigo(i)  + weight_h(j, i, l) * sigi(j)
        end do
     end do

     ! apply activation function
     call actfunc_d( act_h, sigo, sigod, sigi, sigid )

  end do

  ! propagate signal to output layer ...........................................
  do i=1,n_output
     sigod(i) = 0.0
     sigo(i)  = bias_o(i)
     do j=1,n_nodes
        sigod(i) = sigod(i) + weight_o(j, i) * sigid(j)
        sigo(i)  = sigo(i)  + weight_o(j, i) * sigi(j)
     end do
  end do

  ! apply activation function
  call actfunc_d( act_o, sigo(1:n_output), sigod(1:n_output), y, yd )

#ifdef DBG
  print *, 'tangent y = ', y, ',   yd = ', yd
#endif

contains

  pure subroutine actfunc_d( functype, so, sod, si, sid )

    ! activation functions (nonlinear + tangent linear version)

    integer, intent(in) :: functype

    real(sp), dimension(:),    intent(in) :: so, sod
    real(sp), dimension(:), intent(inout) :: si, sid

    select case (functype)
    case (0)               ! linear
       sid = sod
       si  = so

    case (1)               ! relu
       where( so .gt. 0 )
          sid = sod
          si  = so
       elsewhere
          sid = 0.0
          si  = 0.0
       endwhere

    case (2)               ! elu
       where( so .gt. 0 )
          sid = sod
          si  = so
       elsewhere
          sid = sod * exp(so)
          si  =       exp(so) - 1.0
       endwhere

    case (3)               ! softplus
       sid = (sod*exp(so)) / ( exp(so) + 1.0 )
       si  =              log( exp(so) + 1.0 )

    case (4)               ! tanh
       sid = sod * (1.0-tanh(so)**2)
       si = tanh(so)

    case (99) ! csu
       where( so .gt. 0 )
          si  = so
          sid = sod
       elsewhere( so .lt. -2 )
          si = -1.0
          sid = 0.0
       elsewhere
          si  = -1.0 + 0.25*(so+2)**2
          sid =        0.5 *(so+2)*sod
       endwhere 

    case default
       sid = -999.999
       si  = -999.999
    end select

  end subroutine actfunc_d

end subroutine fornado_inference_d

