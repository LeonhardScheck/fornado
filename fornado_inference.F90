!----------------------------------------------------------------------------------------------
!
!   FORNADO NEURAL NETWORK INFERENCE
!   2019-10 L.Scheck
!
!   to generate a python module from this subroutine use
!     f2py -c fornado_inference.F90 -m fornado_inference_f90 --f90flags="-O3 -DVECTORIZE"
!   (for a debug version, use
!     f2py -c fornado_inference.F90 -m fornado_inference_f90 --f90flags="-g -fcheck='all' -DDBG -DVECTORIZE"
!   (see http://cens.ioc.ee/projects/f2py2e/usersguide/f2py_usersguide.pdf )
!
!----------------------------------------------------------------------------------------------

subroutine fornado_inference( n_samples,                             &
                               n_input, n_output, n_hidden, n_nodes,  &
                                         act_h,    act_o,             &
                               weight_i, weight_h, weight_o,          &
                               bias_i,   bias_h,   bias_o,            &
                               x, y, nchunk )

      ! fast replacement for tensorflows predict method

      implicit none
      integer, parameter :: sp = kind(1.0)

      integer, intent(in) :: n_samples, n_input, n_output, n_hidden, n_nodes

      integer, intent(in) :: act_h, act_o

      real(sp), dimension( n_input, n_nodes ),             intent(in) :: weight_i
      real(sp), dimension( n_nodes, n_nodes, n_hidden-1 ), intent(in) :: weight_h
      real(sp), dimension( n_nodes, n_output ),            intent(in) :: weight_o

      real(sp), dimension( n_nodes ),                      intent(in) :: bias_i
      real(sp), dimension( n_nodes, n_hidden-1 ),          intent(in) :: bias_h
      real(sp), dimension( n_output ),                     intent(in) :: bias_o

      real(sp), dimension( n_samples, n_input  ),          intent(in) :: x
      real(sp), dimension( n_samples, n_output ),       intent(inout) :: y

      integer, optional, intent(in) :: nchunk
      integer                       :: n_chunk

#ifdef VECTORIZE
      real(sp), dimension( :, : ), allocatable :: sigi, sigo
#else
      real(sp), dimension( n_nodes ) :: sigi, sigo
#endif
      integer :: i_sample, i, j, k, l, np, nc, k_start, k_end, k_len

#ifdef VECTORIZE
      ! vectorized version: inner loop over samples ------------------------------

      ! process input in chunks of size n_chunk
      if( present(nchunk) ) then
         n_chunk = nchunk
      else
         n_chunk = 2048
      end if
      allocate( sigi(n_chunk,n_nodes) )
      allocate( sigo(n_chunk,n_nodes) )

      do k_start = 1, n_samples, n_chunk
         k_end = min( k_start + n_chunk - 1, n_samples )
         k_len = 1 + k_end - k_start
         !print *, 'CHUNK ', k_start, k_end, k_len

      ! from input to first hidden layer ........................................
      np = n_input
      nc = n_nodes

      ! loop over nodes of current layer
      do i = 1, nc
         do k = 1, k_len
            sigo(k,i) = bias_i(i)
         end do

         ! loop over nodes of previous layer
         do j = 1, np

            ! loop over samples
            do k = 1, k_len
               sigo(k,i) = sigo(k,i) + weight_i(j,i) * x(k_start+k-1,j)
            end do
         end do
      end do

      ! apply activation function
      call actfunc( act_h, sigo(1:k_len,1:nc), sigi(1:k_len,1:nc) )

      ! propagate signal towards last hidden layer ..............................
      np = n_nodes
      nc = n_nodes
      do l = 1, n_hidden - 1

         ! loop over nodes of current layer
         do i = 1, nc
            do k = 1, k_len
               sigo(k,i) = bias_h(i,l)
            end do

            ! loop over nodes of previous layer
            do j = 1, np

               ! loop over samples
               do k = 1, k_len
                  sigo(k,i) = sigo(k,i) + weight_h(j,i,l) * sigi(k,j)
               end do
            end do
         end do

         ! apply activation function
         call actfunc( act_h, sigo(1:k_len,1:nc), sigi(1:k_len,1:nc) )

      end do ! hidden layer loop

      ! propagate signal to output layer ..........................................
      np = n_nodes
      nc = n_output

      ! loop over nodes of current layer
      do i = 1, nc
         do k = 1, k_len
            sigo(k,i) = bias_o(i)
         end do

         ! loop over nodes of previous layer
         do j = 1, np

            ! loop over samples
            do k = 1, k_len
               sigo(k,i) = sigo(k,i) + weight_o(j,i) * sigi(k,j)
            end do
         end do
      end do

      ! apply activation function
      call actfunc( act_o, sigo(1:k_len,1:nc), sigi(1:k_len,1:nc) )

      y(k_start:k_end,:) = sigi(1:k_len,1:nc)

      end do ! chunk loop

      deallocate( sigi )
      deallocate( sigo )

#else
      ! unvectorized version: inner loop over nodes -------------------------------

      ! loop over all input data sets
      do i_sample = 1, n_samples

         ! propagate signal from input layer to first hidden layer
         !sigo = matmul( transpose(weight_i), x(i_sample,:) ) + bias_i
         do i = 1, n_nodes
            sigo(i) = bias_i(i)
            do j = 1, n_input
               sigo(i) = sigo(i) + weight_i(j,i) * x(i_sample,j)
            end do
         end do

         ! apply activation function
         call actfunc( act_h, sigo, sigi )

         ! propagate signal towards last hidden layer
         do l=1, n_hidden-1

            sigo = matmul( transpose(weight_h(:,:,l)), sigi ) + bias_h(:,l)

            ! apply activation function
            call actfunc( act_h, sigo, sigi )
         end do

         sigo(1:n_output) = matmul( transpose(weight_o(:,:)), sigi ) + bias_o(:)
         ! apply activation function
         call actfunc( act_o, sigo(1:n_output), y(i_sample,:) )

      end do
#endif

contains

  pure subroutine actfunc( functype, so, si )

    ! activation functions

    integer, intent(in) :: functype

#ifdef VECTORIZE
    real(sp), dimension(:,:),    intent(in) :: so
    real(sp), dimension(:,:), intent(inout) :: si
#else
    real(sp), dimension(:),    intent(in) :: so
    real(sp), dimension(:), intent(inout) :: si
#endif

    select case (functype)
    case (0)               ! linear
       si  = so
       
    case (1)               ! relu
#ifdef IFLESS
      si = MAX( 0.0, so )
#else
      where( so .gt. 0 )
         si  = so
      elsewhere
         si  = 0.0
      endwhere
#endif

    case (2)               ! elu
#ifdef IFLESS
      si = MIN( ABS(so), exp(so) - 1.0 )
#else
      where( so .gt. 0 )
         si  = so
      elsewhere
         si  = exp(so) - 1.0
      endwhere
#endif

    case (3)               ! softplus
      si  = log( exp(so) + 1.0 )

    case (99) ! csu
#ifdef IFLESS
      si = MIN( ABS(so), -1.0 + 0.25*MAX(0.0,so+2)**2 )
#else
     where( so .gt. 0 )
        si  = so
     elsewhere( so .lt. -2 )
        si = -1.0
     elsewhere
        si  = -1.0 + 0.25*(so+2)**2
     endwhere
#endif

    case (4)               ! tanh
      si = tanh(so)
   
    case default
       si  = -999.999
    end select

  end subroutine actfunc

end subroutine fornado_inference

